/*
 * Copyright (C) 2023  Alfred Neumayer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Box64AndWine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QString>
#include <QVariant>

#include <stdio.h>
#include <unistd.h>

#include <chrono>
#include <thread>

#include "featuremanager.h"

FeatureManager::FeatureManager()
{
    QObject::connect(&m_thread, &QThread::started, this, &FeatureManager::run, Qt::DirectConnection);
}

bool FeatureManager::enabled()
{
    m_enabled = QFile::exists("/home/phablet/.cache/morph-browser/libwidevinecdm.so");

    return m_enabled;
}

bool FeatureManager::enable()
{
    m_thread.start();

    return true;
}

bool FeatureManager::disable()
{
    return false;
}

void FeatureManager::run()
{
    const QByteArray prefContents = QByteArrayLiteral("Package: *\nPin: release o=UBports,a=focal_-_morph-drm\nPin-Priority: 3001");
    const QByteArray repoContents = QByteArrayLiteral("deb http://repo.ubports.com/ focal_-_morph-drm main");

    m_commandRunner->sudo(QStringList{"/usr/bin/mount", "-o", "remount,rw", "/"}, true);

    m_commandRunner->writeFile("/etc/apt/preferences.d/ubports-focal_-_morph-drm.pref", prefContents);
    m_commandRunner->writeFile("/etc/apt/sources.list.d/ubports-focal_-_morph-drm.list", repoContents);

    // BAAAAH we need to patch glibc, here be shit in the fan
    m_commandRunner->sudo(QStringList{"/usr/bin/apt", "update"}, true);
    m_commandRunner->sudo(QStringList{"/usr/bin/apt", "install", "-y", "libc6"}, true);

    m_commandRunner->sudo(QStringList{"-u", "phablet", "mkdir", "-p", "/home/phablet/.cache/morph-browser"}, true);

    // This is just a binary taken from raspbian and aligned with https://gist.github.com/DavidBuchanan314/c6b97add51b97e4c3ee95dc890f9e3c8
    m_commandRunner->sudo(QStringList{"-u", "phablet", "wget", "-O", "/home/phablet/.cache/morph-browser/libwidevinecdm.so", "https://www.dropbox.com/scl/fi/1xotyzodcf2rthk2lcok8/libwidevinecdm.so?rlkey=p62lwft00lnv40kemfbz5jn82&dl=1"}, true);
    m_commandRunner->sudo(QStringList{"wget", "-O", "/etc/profile.d/qtwebengine-gpu.sh", "https://gitlab.com/ubports/development/core/hybris-support/lxc-android-config/-/raw/ubports/focal_-_morph-drm/etc/profile.d/qtwebengine-gpu.sh"}, true);

    m_commandRunner->sudo(QStringList{"/usr/bin/mount", "-o", "remount,ro", "/"}, true);

    m_commandRunner->sudo(QStringList{"/usr/bin/sync"}, true);
    m_commandRunner->sudo(QStringList{"/usr/sbin/reboot"}, true);
}