# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the widenabler.mariogrip package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: widenabler.mariogrip\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-21 22:35+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:132
msgid "widenabler"
msgstr ""

#: ../qml/Main.qml:137
msgid "Info"
msgstr ""

#: ../qml/Main.qml:166
msgid "Enable WideVine DRM"
msgstr ""

#: ../qml/Main.qml:166
msgid "Re-enable WideVine DRM"
msgstr ""

#: widenabler.desktop.in.h:1
msgid "Widenabler"
msgstr ""
